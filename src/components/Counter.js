import React from 'react';
import Loader from './Loader';

class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: 0 };
  }

  handleChange = (event) => {
    let number = parseInt(event.target.value);
    this.setState({ value: number });
  }

  handleClick = () => {
    console.log(this.state.value);
    console.log(typeof this.state.value)

    this.props.add(this.state.value)
  }

  fetchRandomNumber = async () => {
    await this.props.addFetchData();
    this.setState({ value: this.props.randomNumber })
  };

  render() {
    return (
      <div>
        <p> {this.props.counter} </p>
        <button onClick={this.props.increment}> + </button>
        <button onClick={this.props.decrement}> - </button>

        <input type="text" value={this.state.value} onChange={this.handleChange} />
        <button onClick={this.handleClick}> Add </button>
        <button onClick={this.fetchRandomNumber}> Fetch </button>
        {this.props.randomNumber && 
          <strong> 
            Random Number: {this.props.randomNumber} 
          </strong>
        }
        <ul>{this.props.fetchedHistory &&this.props.fetchedHistory.map(
          (item, index, key) => {
            return <li>{item}</li>
          }
        )}
        </ul>
        {this.props.loader && 
        <Loader />
        }
      </div>
    );
  }
}

export default Counter;