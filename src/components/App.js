import '../index.css';
import React from 'react';
import CounterContainer from '../containers/CounterContainer';

const App = (props) => {
  return(
        <div>
          { props.toggle ?
            <div className="menu">
              <a href="https://www.google.com" target="_blank">Google</a>
            </div> :
            '' }
          <div>
            <button onClick={() => props.onClickButton()}>Toggle menu!</button>
          </div>
          <CounterContainer />
        </div>
        );
}

export default App;