import onClickButton from '../actions/toogle'
import App from '../components/App';
import { connect, state, dispatch } from 'react-redux';
import { bindActionCreators } from 'redux';

const mapStateToProps = state => ({
  toggle: state.toggleReducer.toggle
})

const mapDispatchToProps = dispatch => {
return bindActionCreators({onClickButton}, dispatch);
};

const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppContainer;