import Counter from '../components/Counter';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { increment, decrement, add, fetchData, addFetchData } from '../actions/counter';

const mapStateToProps = state => ({
  counter: state.counterReducer.counter,
  randomNumber: state.counterReducer.randomNumber,
  loader: state.loaderReducer.loader,
  fetchedHistory: state.counterReducer.fetchedHistory
})

const mapDispatchToProps = dispatch => {
return bindActionCreators({increment, decrement, add, fetchData, addFetchData}, dispatch);
};

const CounterContainer = connect(mapStateToProps, mapDispatchToProps)(Counter);

export default CounterContainer;