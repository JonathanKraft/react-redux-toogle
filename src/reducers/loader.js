const INITIAL_STATE = {
  loader: false
};

//Implement the reducer
function loaderReducer (state = INITIAL_STATE, action){
  switch (action.type) {
    case 'TOGGLE_LOADER':
      return {
        loader: action.value
      };
    default:
      return state;
  }
}

export default loaderReducer;