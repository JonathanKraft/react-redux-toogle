import { createStore, combineReducers, applyMiddleware } from 'redux';
import toggleReducer from './reducers/toogle';
import counterReducer from './reducers/counter';
import loaderReducer from './reducers/loader';
import thunk from 'redux-thunk';

const store = createStore(combineReducers({toggleReducer, counterReducer, loaderReducer}), applyMiddleware(thunk));

export default store;