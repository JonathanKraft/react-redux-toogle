import { toggleLoader } from "./loader";

export const increment = () => {
  return { type: 'INCREMENT' }
}

export const decrement = () => {
  return { type: 'DECREMENT' }
}

export const add = (value) => {
  console.log(typeof value);
  
  return { type: 'ADD', value: value }
}

export const fetchData = (value) => {
  return { type: 'FETCH_DATA', value: value }
}

export const addFetchData = (dispatch) => {
  return async (dispatch) => {
    dispatch(toggleLoader(true));
    const response = await fetch("https://qrng.anu.edu.au/API/jsonI.php?length=1&type=uint8");
    const data = await response.json();
    dispatch(toggleLoader(false));
    let value = parseInt(data.data[0])
    dispatch(fetchData(value));
  }
}
